﻿using System;
using System.Collections.Generic;
using System.Text;
using Métodos_Abstratos.Entities.Enums;

namespace Métodos_Abstratos.Entities
{
    class Circle : Shape
    {
        public double Radius { get; set; }

        public Circle(double radius, Color color) : base(color)
        {
            Radius = radius;
        }

        public override double Area()
        {
            return Math.PI * Math.Pow(Radius, 2);
        }
    }
}
