﻿using System;
using System.Collections.Generic;
using System.Text;
using Métodos_Abstratos.Entities.Enums;

namespace Métodos_Abstratos.Entities
{
    abstract class Shape
    {
        public Color Color { get; set; }

        public Shape(Color color)
        {
            Color = color;
        }

        public abstract double Area();
    }
}
