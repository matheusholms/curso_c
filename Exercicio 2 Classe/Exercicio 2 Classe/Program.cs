﻿using System;

namespace Exercicio_2_Classe {
    class Program {
        static void Main(string[] args) {
            Funcionario funcionario1, funcionario2;
            funcionario1 = new Funcionario();
            funcionario2 = new Funcionario();

            Console.WriteLine("Dados do primeiro funcionário");
            Console.Write("Nome: ");
            funcionario1.nome = Console.ReadLine();
            Console.Write("Salário: ");
            funcionario1.salario = double.Parse(Console.ReadLine());

            Console.WriteLine("Dados do segundo funcionário");
            Console.Write("Nome: ");
            funcionario2.nome = Console.ReadLine();
            Console.Write("Salário: ");
            funcionario2.salario = double.Parse(Console.ReadLine());

            if (funcionario1.salario > funcionario2.salario)
                Console.WriteLine("Salário do " + funcionario1.nome + " é maior");
            else
                Console.WriteLine("Salário do " + funcionario2.nome + " é maior");
        }
    }
}
