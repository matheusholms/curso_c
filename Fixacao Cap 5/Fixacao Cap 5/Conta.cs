﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace Fixacao_Cap_5 {
    class Conta {

        public int Id { get; private set; }
        public string Nome { get; set; }
        public double Saldo { get; private set; }

        public Conta (int id, string nome) {
            Id = id;
            Nome = nome;
        }

        public void Deposito(double valorDeposito) {
            Saldo += valorDeposito;
        }

        public void Saque(double valorSaque) {
            Saldo -= valorSaque + 5.0;
        }

        public override string ToString() {
            return
                "Conta "
                + Id
                + ", Titular: "
                + Nome
                + ", Saldo: R$"
                + Saldo.ToString("F2", CultureInfo.InvariantCulture);
        }
    }
}
