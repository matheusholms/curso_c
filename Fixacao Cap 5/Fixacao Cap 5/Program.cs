﻿using System;

namespace Fixacao_Cap_5 {
    class Program {
        static void Main(string[] args) {

            int Id;
            string Nome;
            double Valor; 

            Console.Write("Entre o número da conta:");
            Id = int.Parse(Console.ReadLine());
            Console.Write("Entre o titular da conta:");
            Nome = Console.ReadLine();

            Conta conta = new Conta(Id, Nome);

            Console.Write("Haverá depósito inicial? (s/n)");
            char Condicao = char.Parse(Console.ReadLine().ToUpper());
            if (Condicao == 'S') {
                Console.Write("Entre o valor de depósito inicial: ");
                Valor = double.Parse(Console.ReadLine());
                conta.Deposito(Valor);    
            }

            Console.WriteLine();
            Console.WriteLine("Dados da conta: ");
            Console.WriteLine(conta);
            Console.WriteLine();

            Console.Write("Entre o valor para depósito: ");
            Valor = double.Parse(Console.ReadLine());
            conta.Deposito(Valor);
            Console.WriteLine("Dados da conta atualizados:");
            Console.WriteLine(conta);
            Console.WriteLine();

            Console.Write("Entre o valor para saque: ");
            Valor = double.Parse(Console.ReadLine());
            conta.Saque(Valor);
            Console.WriteLine("Dados da conta atualizados:");
            Console.WriteLine(conta);
            Console.WriteLine();


        }
    }
}
