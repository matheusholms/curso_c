﻿using System;

namespace Matriz {
    class Program {
        static void Main(string[] args) {

            int n = int.Parse(Console.ReadLine());

            int[,] mat = new int[n, n];
            //Corre as linhas
            for (int i = 0; i < n; i++) {

                string[] values = Console.ReadLine().Split(' ');

                //Corre as colunas
                for (int j = 0; j < n; j++) {
                    mat[i, j] = int.Parse(values[j]);
                }

                
            }

            Console.WriteLine("Diagonal principal:");
            for (int i = 0; i < n; i++ ) {
                Console.Write(mat[i,i] + " ");
            }

            int count = 0;
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    if (mat[i, j] < 0) {
                        count++;
                    }
                }
            }
            Console.WriteLine("Negative numbers: " + count);
        }
    }
}
