﻿
static void Main(string[] args)
{
    string sourcePath = @"c:\temp\file1.txt";
    string targetPath = @"c:\temp\file2.txt";

    try
    {
        FileInfo fileInfo = new FileInfo(sourcePath);
        fileInfo.CopyTo(targetPath);
    } 
    catch (IOException e)
    {
        Console.WriteLine("An error occurred");
        Console.WriteLine(e.Message);
    }


}